----
author: intronaut
created: 2023-11-23
updated: 2023-11-23
----

## Grief


- **grief is pain**
    + intensity of pain by grief varies wildy person-to-person, as well as by temporal or geographic borders.
        * on the ugly side is also **never** a measure of any situation on its own.
- **grief experienced through pain, should not be judged by others.**
- **grief is permanent**
    + grief experienced through pain is permanent. However, typically not present in stable intensivity, but rather gradually decreasing over varying amounts of time, but eventually diminishing. (amount of diminishing or experiencing grief vary person-to-person in their own regard).
- **grief is personal**
- **grief is an universal human experience**
- **caused by either loss of a positive, or gain of a negative**, pertaining to e.g. real-world objects or a cultural tradion event; very broadly: _psychotraumatic events, undeniably associated within certain context._
- **as grief is _undeniably associated within certain context._** ; after original experience, _similar context in the future_ are prone to evoke the original reaction of the psychotraumatic victim, essentially reflexively, although maybe not suitable with regard to the _actual current context and/or severity of psychotrauma-associated intensity of pain associated with this context._ that's about as far as I'm going towards PTSD.

### Typicalities

Rounding off some edges in providing examples

#### (Permanent) loss of a person/person(s)

- Most stereotypipcal event related grief ; the loss of a person defined as either the (permanent) removal of an important close-circle individual out of the victims currently active life. Either by will of the other, or causes of death or law
   - deceased relative/close-circle
   - abandonment, heartbreak
   - incarceration or deportment
   - death

#### Grief of the non-existing

- Interestingly, grief is not always directly related to context _actually and primarily experienced by the victim_. There are rough assets on which typology can depend:
   - 1. Both the grief mechanism as well as experienced pain could be projected by oneself (knowingly or not) as well as by another effector (i.e. _by proxy_, being much more rare)
   - 2. Childhood trauma, or better: psychotraumata in developmental age (such as childhood, puberty, adolescence). Lacking certain social needs and teachings (refer to: _loss of a positive_), can fundamental harm the social development in any individual. Damaged social development will oftentime remain in someone's psycho-defensive capabilities, far into adulthood. As such, individuals canreflexively
